package org.keycloak.protocol.cas;

import org.junit.Test;
import org.keycloak.dom.saml.v2.protocol.ResponseType;
import org.keycloak.protocol.cas.representations.CASErrorCode;
import org.keycloak.protocol.cas.representations.SamlResponseHelper;
import org.keycloak.protocol.cas.utils.CASValidationException;
import org.keycloak.protocol.cas.XMLValidator;
import org.w3c.dom.Document;

import javax.ws.rs.core.Response;
import java.util.Collections;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class SamlResponseTest {
    @Test
    public void successResponseIsWrappedInSOAP() {
        ResponseType response = SamlResponseHelper.successResponse("keycloak", "test@example.com", Collections.emptyMap());
        String soapResult = SamlResponseHelper.soap(response);
        assertTrue(soapResult.contains("samlp:Success"));
        assertTrue(soapResult.contains("test@example.com"));
        assertTrue(soapResult.contains("keycloak"));
    }

    @Test
    public void failureResponseIsWrappedInSOAP() {
        ResponseType response = SamlResponseHelper.errorResponse(new CASValidationException(CASErrorCode.INVALID_TICKET, "Nope", Response.Status.BAD_REQUEST));
        String nope = SamlResponseHelper.soap(response);
        assertTrue(nope.contains("Nope"));
    }

    @Test
    public void validateSchemaResponseFailure() throws Exception {
        ResponseType response = SamlResponseHelper.errorResponse(new CASValidationException(CASErrorCode.INVALID_TICKET, "Nope", Response.Status.BAD_REQUEST));
        String output = SamlResponseHelper.toString(SamlResponseHelper.toDOM(response));
        Document doc = XMLValidator.parseAndValidate(output, XMLValidator.schemaFromClassPath("oasis-sstc-saml-schema-protocol-1.1.xsd"));
        assertNotNull(doc);
    }

    @Test
    public void validateSchemaResponseSuccess() throws Exception {
        ResponseType response = SamlResponseHelper.successResponse("keycloak", "test@example.com", Collections.emptyMap());
        String output = SamlResponseHelper.toString(SamlResponseHelper.toDOM(response));
        Document doc = XMLValidator.parseAndValidate(output, XMLValidator.schemaFromClassPath("oasis-sstc-saml-schema-protocol-1.1.xsd"));
        assertNotNull(doc);
    }
}
