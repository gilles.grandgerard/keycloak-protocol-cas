package org.keycloak.protocol.cas.representations;

import java.io.StringWriter;
import java.net.URI;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Collection;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.keycloak.dom.saml.v2.assertion.AssertionType;
import org.keycloak.dom.saml.v2.assertion.AttributeStatementType;
import org.keycloak.dom.saml.v2.assertion.AttributeType;
import org.keycloak.dom.saml.v2.assertion.AuthnStatementType;
import org.keycloak.dom.saml.v2.assertion.ConditionsType;
import org.keycloak.dom.saml.v2.assertion.NameIDType;
import org.keycloak.dom.saml.v2.assertion.StatementAbstractType;
import org.keycloak.dom.saml.v2.assertion.SubjectType;
import org.keycloak.dom.saml.v2.protocol.ResponseType;
import org.keycloak.dom.saml.v2.protocol.StatusCodeType;
import org.keycloak.dom.saml.v2.protocol.StatusType;
import org.keycloak.protocol.cas.utils.CASValidationException;
import org.keycloak.saml.SAML2ErrorResponseBuilder;
import org.keycloak.saml.common.exceptions.ProcessingException;
import org.keycloak.saml.processing.api.saml.v2.response.SAML2Response;
import org.keycloak.saml.processing.core.saml.v2.factories.JBossSAMLAuthnResponseFactory;
import org.keycloak.saml.processing.core.saml.v2.writers.SAMLResponseWriter;
import org.keycloak.services.validation.Validation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class SamlResponseHelper {
    private final static DatatypeFactory factory;

    static {
        try {
            factory = DatatypeFactory.newInstance();
        } catch (DatatypeConfigurationException e) {
            throw new RuntimeException(e);
        }
    }

    public static ResponseType errorResponse(CASValidationException ex) {
// GG    	return JBossSAMLAuthnResponseFactory.createResponseType("_" + UUID.randomUUID().toString(), 
//    			                                                "urn:oasis:names:tc:SAML:2.0:status:RequestDenied")
//    			                                                );
// GG   	return new SAML2ErrorResponseBuilder()
//    			      .status("urn:oasis:names:tc:SAML:2.0:status:RequestDenied")
//    	              .buildDocument();
//    	
        ZonedDateTime nowZoned = ZonedDateTime.now(ZoneOffset.UTC);
        XMLGregorianCalendar now = factory.newXMLGregorianCalendar(GregorianCalendar.from(nowZoned));

        return applyTo(new ResponseType("_" + UUID.randomUUID().toString(), now), obj -> {
            obj.setStatus(applyTo(new StatusType(), status -> {
                //status.setStatusCode(new StatusCodeType(QName.valueOf("samlp:RequestDenied")));
            	status.setStatusCode(applyTo( new StatusCodeType(), statusCode -> { 
            			statusCode.setValue( URI.create("urn:oasis:names:tc:SAML:2.0:status:RequestDenied")); 
            			}));
                status.setStatusMessage(ex.getErrorDescription());
            }));
        });
    }

    public static ResponseType successResponse(String issuer, String username, Map<String, Object> attributes) {
//GG        return JBossSAMLAuthnResponseFactory.createResponseType("_" + UUID.randomUUID().toString(), "urn:oasis:names:tc:SAML:2.0:status:RequestDenied")
//                 	JBossSAMLAuthnResponseFactory.createResponseType();
// idée saml2 !    	SAML2Response.createResponseType() ;
    	
    	ZonedDateTime nowZoned = ZonedDateTime.now(ZoneOffset.UTC);
        XMLGregorianCalendar now = factory.newXMLGregorianCalendar(GregorianCalendar.from(nowZoned));
        
        return applyTo(new ResponseType("_" + UUID.randomUUID().toString(), now),
                obj -> {
                    obj.setStatus(applyTo(new StatusType(), status -> status.setStatusCode(StatusCodeType.SUCCESS)));
                    obj.add(applyTo(new AssertionType("_" + UUID.randomUUID().toString(), now), assertion -> {
                        assertion.setIssuer(issuer);
                        assertion.setConditions(applyTo(new ConditionsType(), conditions -> {
                            conditions.setNotBefore(now);
                            conditions.setNotOnOrAfter(factory.newXMLGregorianCalendar(GregorianCalendar.from(nowZoned.plusMinutes(5))));
                        }));
                        assertion.add(applyTo(new AuthenticationStatementType(
                                URI.create(
                                		"urn:oasis:names:tc:SAML:1.0:am:password"
                                		//Constants.AUTH_METHOD_PASSWORD
                                		),
                                now
                        ), stmt -> stmt.setSubject(toSubject(username))));
                        assertion.addAllStatements(toAttributes(username, attributes));
                    }));
                }
        );
    }

    private static List<StatementAbstractType> toAttributes(String username, Map<String, Object> attributes) {
        List<AttributeType> converted = attributeElements(attributes);
        if (converted.isEmpty()) {
            return Collections.emptyList();
        }
        return Collections.singletonList(applyTo(
                new AttributeStatementType(),
                attrs -> {
                    attrs.setSubject(toSubject(username));
                    attrs.addAllAttributes(converted);
                })
        );
    }

    private static List<AttributeType> attributeElements(Map<String, Object> attributes) {
        return attributes.entrySet().stream().flatMap(e ->
                toAttribute(e.getKey(), e.getValue())
        ).filter(a -> !a.get().isEmpty()).collect(Collectors.toList());
    }

    private static Stream<AttributeType> toAttribute(String name, Object value) {
        if (name == null || value == null) {
            return Stream.empty();
        }

        if (value instanceof Collection) {
            return Stream.of(samlAttribute(name, listString((Collection<?>) value)));
        }
        return Stream.of(samlAttribute(name, Collections.singletonList(value.toString())));
    }

    private static AttributeType samlAttribute(String name, List<Object> listString) {
        return applyTo(
                new AttributeType(name, URI.create("http://www.ja-sig.org/products/cas/")),
                attr -> attr.addAll(listString)
        );
    }

    private static List<Object> listString(Collection<?> value) {
        return value.stream().map(Object::toString).collect(Collectors.toList());
    }

    private static SubjectType toSubject(String username) {
        return applyTo(
                new SubjectType(),
                subject -> subject.setChoice(
                        new SubjectType.STSubType(
                                applyTo(
                                        new NameIDType(username),
                                        ctype -> ctype.setFormat(nameIdFormat(username))
                                )
                        )
                )
        );
    }

    private static URI nameIdFormat(String username) {
        return URI.create(Validation.isEmailValid(username) ?
                //Constants.FORMAT_EMAIL_ADDRESS 
        		"urn:oasis:names:tc:SAML:1.1:nameid-­format:emailAddress"
        		:
        		//Constants.FORMAT_UNSPECIFIED
    			"urn:oasis:names:tc:SAML:1.1:nameid­-format:unspecified"
        );
    }

    private static <A> A applyTo(A input, Consumer<A> setter) {
        setter.accept(input);
        return input;
    }

    public static String soap(ResponseType response) {
        try {
            Document result = toDOM(response);

            Document doc = wrapSoap(result.getDocumentElement());
            return toString(doc);

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static Document toDOM(ResponseType response) throws ParserConfigurationException, XMLStreamException, ProcessingException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);

        XMLOutputFactory factory = XMLOutputFactory.newFactory();

        Document doc = dbf.newDocumentBuilder().newDocument();
        DOMResult result = new DOMResult(doc);
        XMLStreamWriter xmlWriter = factory.createXMLStreamWriter(result);
        SAMLResponseWriter writer = new SAMLResponseWriter(xmlWriter);
        writer.write(response);
        return doc;
    }

    private static Document wrapSoap(Node node) throws ParserConfigurationException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        Document doc = dbf.newDocumentBuilder().newDocument();

        Element envelope = doc.createElementNS("http://schemas.xmlsoap.org/soap/envelope/", "soap:Envelope");
        envelope.appendChild(doc.createElementNS("http://schemas.xmlsoap.org/soap/envelope/", "soap:Header"));
        Element body = doc.createElementNS("http://schemas.xmlsoap.org/soap/envelope/", "soap:Body");

        Node imported = doc.importNode(node, true);

        body.appendChild(imported);
        doc.appendChild(body);
        envelope.appendChild(body);
        doc.appendChild(envelope);
        return doc;
    }

    public static String toString(Document document) throws TransformerException {
        // Output the Document
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer t = tf.newTransformer();
        DOMSource source = new DOMSource(document);
        StringWriter writer = new StringWriter();
        StreamResult result = new StreamResult(writer);
        t.transform(source, result);
        return writer.toString();
    }
}
