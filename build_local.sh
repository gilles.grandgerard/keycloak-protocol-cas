#!/bin/bash -x

SCRIPT_PATH="${BASH_SOURCE[0]}"
if [ -h "${SCRIPT_PATH}" ]
then
  while [ -h "${SCRIPT_PATH}" ]
  do 
      SCRIPT_PATH=$(readlink "${SCRIPT_PATH}")
  done
fi
pushd . > /dev/null
DIR_SCRIPT=$(dirname "${SCRIPT_PATH}" )
cd "${DIR_SCRIPT}" > /dev/null || exit 1
SCRIPT_PATH=$(pwd);
popd  > /dev/null || exit 1

WORKSPACE=$SCRIPT_PATH
if [ -z "$JAVA_HOME" ]
then
    JAVA_HOME=/usr/lib/jvm/java-11-openjdk-amd64
    JAVA_HOME=/home/gilles/jdk1.7.0_121
    JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
fi
export JAVA_HOME

if [ -z "$M2_HOME" ]
then
    M2_HOME=~/.m2
fi

if [ "$LOGNAME" == "jenkins" ]
then
    MAVEN_HOME=/var/lib/jenkins/tools/hudson.tasks.Maven_MavenInstallation/maven_3.6.3
fi
if [ -z "$MAVEN_HOME" ]
then
    MAVEN_HOME=~/apache-maven-3.6.3
fi
export MAVEN_HOME

MAVEN_PATH=$MAVEN_HOME/bin
PATH=$MAVEN_HOME/bin:$JAVA_HOME/bin:$PATH
export PATH

echo "WORKSPACE: $WORKSPACE"
echo "MAVEN_HOME: $MAVEN_HOME"
echo "JAVA_HOME: $JAVA_HOME"
echo "PATH: $PATH"
$JAVA_HOME/bin/java -version

pushd . > /dev/null
cd "${WORKSPACE}" > /dev/null || exit 1
mvn -e clean compile install 
popd  > /dev/null || exit 1
